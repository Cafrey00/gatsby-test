import React, {useState, useEffect} from 'react';
import styled from "styled-components"

const StyledGameWrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    border: 1px solid red;
`;
const StyledGameCell = styled.button`
    height: 200px;
    width: 33.333%; 
    background-color: #fff;
    font-family: 'Tahoma';
    font-size: 90px;
    font-weight: 900;

    span {
        transform: scale(0);
        transition: all 2s ease;
        &.visible {
            transform: scale(1);
        }
    }

    &.circle {
        color: blue;
    }

    &.cross {
        color: red;
    }
`;

const tab = [{
    age: 15,
    name: 'Marcin',
}, {
    age: 155,
    name: 'Michał',
}, {
    age: 25,
    name: 'Sławek',
}];
 
const CircleAndCross = ({age}) => { 
    const [isCross, setIsCross] = useState(false);
    const [gameTable, setGameTable] = useState([null,null,null,null,null,null,null,null,null]);

   useEffect(() => {
       console.log('start');   
       return () => {
           console.log('end')
       }
   }, [gameTable]);
    
    const onCellClick = (index) => {
        if(gameTable[index] !== null) {
            return;
        }
        setGameTable([
            ...gameTable.slice(0, index),
            isCross ? 'x' : 'o',
            ...gameTable.slice(index + 1)
        ])
        setIsCross(!isCross)
    }
    
    return (
        <StyledGameWrapper> 
            {gameTable.map((cell, k) => {
                const cellClass = (cell === 'o' ? 'circle' : (cell === 'x' ? 'cross' : null)); 
                return (
                    <StyledGameCell onClick={() => onCellClick(k)} key={k} className={cellClass}>
                        <span className={cell ? 'visible' : null}>{ cell }</span>
                    </StyledGameCell>
                )
            })}
        </StyledGameWrapper>
    ) 
}

export default CircleAndCross;