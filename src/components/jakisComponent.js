import React, {useState} from 'react';
import styled from 'styled-components';

const StyledHeader = styled.h1`
    background-color: red;
    &.jakas-klasa {
        color: blue;
    }
`   

const JakisComponent = ({ headerText }) => {
    const [iterator, setIterator] = useState(0);
    


    const onClick = () => {
        setIterator(iterator + 1);
    }

    return (
        <StyledHeader onClick={onClick} className="jakas-klasa">{headerText} {iterator}</StyledHeader>
    )
}

export default JakisComponent;