import React from 'react'
import Layout from '../components/layout'
import { Link } from 'gatsby'

const Page100 = () => {
    return (
        <Layout>
            <h1>Nowa strona</h1>
            <Link to="/">Homepage</Link>
        </Layout>
    )
}

export default Page100;